# Release Notes

## ver 1.1

Feb 17, 2020

- Fixed rendering issue when loading files with same name in a row

## ver 1.0

Nov 14, 2020

- Changed platform from Github to Gitlab
- Changed project architecture
- Changed theme

## ver 0.2

May 20, 2020

- Minor change in UI

## ver 0.1

May 19, 2020

- Removed Print button and load url mode
- Changed wordings
- Introduced collapse effect
- Introduced CSS for print

## ver 0.0

May 18, 2020

- Introduced On Your Mark
