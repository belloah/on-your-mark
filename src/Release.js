import packagejson from '../package.json'
const Release = (props) => <small>Copyright &copy; {props.name} <a title="More about this project" href={"https://gitlab.com/belloah" + packagejson.homepage} target="_blank" rel="noreferrer">ver {packagejson.version}</a></small>
export default Release