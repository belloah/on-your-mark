import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Collapse from 'react-bootstrap/Collapse';
import { useState } from 'react';
// import { useEffect } from 'react';
import mark from 'marked/lib/marked';
import sample from './homkaling.md';
const App = () => {
    const [reader, setReader] = useState(false)
    const [resume, setResume] = useState("")
    const loadfile = (e) => {
        let files = e.target.files;
        let file = files[0];
        if (file.name.endsWith(".md")) {
            let reader = new FileReader();
            reader.onload = (event) => {
                setResume(<div className="resume card card-body my-2" dangerouslySetInnerHTML={{ __html: mark(event.target.result) }} />);
            }
            reader.readAsText(file);
            e.target.value = null
        }
        else {
            alert("Please upload a markdown file");
        }
    }
    const homkaling = () => {
        fetch(sample).then(response => response.text().then(text => setResume(<div className="resume card card-body my-2" dangerouslySetInnerHTML={{ __html: mark(text) }} />)))
        setReader(false)
    }
    // useEffect(() => console.log(sample))
    return (<>
        <Navbar collapseOnSelect expand="lg" bg="light" className="border my-2" sticky="top">
            <Navbar.Brand className="d-block d-lg-none"><i className="far fa-file" /> Make your resume</Navbar.Brand>
            <Navbar.Toggle aria-controls="navbarNav" />
            <Navbar.Collapse id="navbarNav">
                <div className="navbar-nav">
                    <Nav.Link href="#" onClick={homkaling}>Load and view an example</Nav.Link>
                    <Nav.Link href={sample} download="template.md">Download a template</Nav.Link>
                    <Nav.Link href="#" onClick={() => setReader(true)}>Load your file</Nav.Link>
                </div>
            </Navbar.Collapse>
        </Navbar>
        <Collapse in={reader}>
            <div className="custom-file">
                <label htmlFor="file" className="custom-file-label">Load your file here</label>
                <input type="file" id="file" name="file" className="custom-file-input" formEncType="multipart/form-data" onChange={loadfile} />
            </div>
        </Collapse>
        {resume}
    </>)
}
export default App