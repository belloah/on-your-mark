import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'bootswatch/dist/journal/bootstrap.min.css';
import '@fortawesome/fontawesome-free/css/all.css';
import Footer from './Footer';
import Main from './Main';
import Release from './Release';
import Title from './Title';
import App from './App';
// import reportWebVitals from './reportWebVitals';

const name = "On Your Mark"

ReactDOM.render(
  <>
    <Main>
      <Title>{name}</Title>
      <App />
    </Main>
    <Footer>
      <Release name={name} />
    </Footer>
  </>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
