const Main = (props) => <main role="main" className="flex-shrink-0"><div className="container">{props.children}</div></main>
export default Main